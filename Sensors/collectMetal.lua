local sensorInfo = {
	name = "collectMetal",
	desc = "gives FARCKs commands to gather metal from battlefield",
	author = "novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spCommand = Spring.GiveOrderToUnit
local spEcho = Spring.Echo
local radius = 500

--returns index of minimal value
function getArgMin(counts)
  local argmin = 1
  local minval = counts[1]
  for i = 2, #counts do
    if counts[i] < minval then
      argmin = i
      minval = counts[i]
    end
  end
  return argmin  
end


return function (places, units)	
  placeCnts = {0,0,0}
  --compute current FARCKs distribution to paths
  for key, value in pairs(bb.farcks) do
    if value > 0 then
      placeCnts[value] = placeCnts[value] + 1
    end
  end
  
  for i = 1, #units do
    if bb.farcks[units[i]] == 0 then --send unassigned units equally
      nextline = getArgMin(placeCnts)
      bb.farcks[units[i]] = nextline
      placeCnts[nextline] = placeCnts[nextline] + 1
      --CMD.MOVE command is necessary - if FARCK does not know about any metal in the place,
      --he will do nothing with CMD.RECLAIM.
      --moreover, we move him near corner, otherwise he will go into center of his area (often into battlefield without necessity)
      spCommand(units[i], CMD.MOVE, {places[nextline].x - 350, places[nextline].y, places[nextline].z + 350},{})     
    	spCommand(units[i], CMD.RECLAIM, {places[nextline].x, places[nextline].y, places[nextline].z, radius},{"shift"})
    end
  end
end
