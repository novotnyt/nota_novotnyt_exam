local sensorInfo = {
	name = "loadPathInfo",
	desc = "Stores refined corridors into bb.paths",
	author = "novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spEcho = Spring.Echo

return function(info)
  bb.paths = {{},{},{}}
  bb.strongholds = {}
	--movement of points with stronghold is required, it is not accessible waypoint
  for key, value in ipairs(info.corridors.Top.points) do    
    bb.paths[1][key] = value.position
    if value.isStronghold then
      bb.paths[1][key].x = bb.paths[1][key].x - 50
      bb.paths[1][key].z = bb.paths[1][key].z + 50
    end
  end
  for key, value in ipairs(info.corridors.Middle.points) do
    bb.paths[2][key] = value.position
    if value.isStronghold then
      bb.paths[2][key].x = bb.paths[1][key].x - 50
      bb.paths[2][key].z = bb.paths[1][key].z + 50
    end
  end
  for key, value in ipairs(info.corridors.Bottom.points) do
    bb.paths[3][key] = value.position
    if value.isStronghold then
      bb.paths[2][key].x = bb.paths[1][key].x - 50
      bb.paths[2][key].z = bb.paths[1][key].z + 50
    end
  end
  
  --broken waypoint fix
  bb.paths[2][7].x = bb.paths[2][7].x + 50
  
  
  --misc for buyItem cost check
  bb.costs = info.buy  
end