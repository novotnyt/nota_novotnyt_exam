local sensorInfo = {
	name = "getAllseers",
	desc = "Returns all owned seers on the map",
	author = "novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return table of all seers
return function()
	local teamID = Spring.GetMyTeamID()
	local allies = Spring.GetTeamUnits(teamID)
	local seers = {}

	local index = 1
	for _,value in pairs(allies) do
		local defID = Spring.GetUnitDefID(value)
		if UnitDefs[defID].humanName == "Seer" then
			seers[#seers+1] = value
		end
	end
	return seers
end