local sensorInfo = {
	name = "attack",
	desc = "Send wave of free Zeuses",
	author = "novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spCommand = Spring.GiveOrderToUnit

return function(unitIDs)
  --cycle through paths during attack wawes
  if bb.attackID == nil then bb.attackID = 0 end  
  pathID = (bb.attackID % 3) + 1
  bb.attackID = bb.attackID + 1
  
  path = bb.paths[pathID]
  for key, unitID in pairs(unitIDs) do 
    --send all unused Zeuses through selected path, mark them as used  
    bb.zeuses[unitID] = "walking"
    spCommand(unitID, CMD.STOP, {}, {})
    for _, point in pairs(path) do 
      spCommand(unitID, CMD.MOVE, {point.x, point.y, point.z}, {"shift"}) 
    end
  end
end