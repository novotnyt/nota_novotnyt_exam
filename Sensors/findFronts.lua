local sensorInfo = {
	name = "findFronts",
	desc = "find places on map, where fighting is happenning",
	author = "Krabec, novotnyt",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local myAllyID = Spring.GetMyAllyTeamID()

return function()
	bb.fronts = {}
	for i = 1,3 do 
		for key,value in pairs(bb.paths[i]) do
			if bb.map[i][key] == "danger" then
				bb.fronts[i] = value
				break
			end
		end
    --handle cases where we know about no enemies
    if bb.fronts[i] == nil then
      bb.fronts[i] = bb.paths[i][5] -- 5 is always deep inside safe area
    end
	end
end