local sensorInfo = {
	name = "getAllseers",
	desc = "Returns all owned seers on the map",
	author = "novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spCommand = Spring.GiveOrderToUnit

-- @description assigns one seer to each corridor, from top to bottom
return function(seers)
	for i = 1, #seers do
    modi = (i % 3) + 1
    spCommand(seers[i], CMD.MOVE, {bb.fronts[modi].x, bb.fronts[modi].y, bb.fronts[modi].z}, {})
  end
end