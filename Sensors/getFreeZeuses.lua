local sensorInfo = {
	name = "getFreeZeuses",
	desc = "Returns all non-sent Zeuses",
	author = "novotnyt",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function checkIfLineIsOccupied(laneId)
	for key,value in pairs(bb.farcks) do
		if value == laneId then return true end
	end
	return false
end

return function()
	local teamID = Spring.GetMyTeamID()
	local allies = Spring.GetTeamUnits(teamID)
	local freeZeuses = {}
	
  --init table
	if bb.zeuses == nil then
		bb.zeuses = {}
	end
	for key,value in pairs(bb.farcks) do
		if not Spring.ValidUnitID(key) then bb.zeuses[key] = nil end
	end
		
	for _,value in pairs(allies) do
		local defID = Spring.GetUnitDefID(value)
    --pick all Zeuses which are not sent yet 
		if UnitDefs[defID].humanName == "Zeus" and bb.zeuses[value] == nil then
      freeZeuses[#freeZeuses + 1] = value			
		end
	end
	return freeZeuses
end