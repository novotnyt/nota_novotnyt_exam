local sensorInfo = {
	name = "getAllFARKS",
	desc = "Returns all owned FARKS",
	author = "Krabec",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spEcho = Spring.Echo
local spGetUnitCommands = Spring.GetUnitCommands

return function()
	local teamID = Spring.GetMyTeamID()
	local allies = Spring.GetTeamUnits(teamID)
	local farcks = {}
	
	if bb.farcks == nil then
		bb.farcks = {}
	end
	for key,value in pairs(bb.farcks) do
		if not Spring.ValidUnitID(key) then bb.farcks[key] = nil end
	end
		
	local index = 1
	for _,value in pairs(allies) do
		local defID = Spring.GetUnitDefID(value)
		if UnitDefs[defID].humanName == "FARCK" then
			farcks[index] = value
			index = index + 1
      --if the fark is not active (i.e. no metal is left in his place), denote him as free
			if bb.farcks[value] == nil or spGetUnitCommands(value, 0) == 0 then
				bb.farcks[value] = 0
			end
		end
	end
	return farcks
end