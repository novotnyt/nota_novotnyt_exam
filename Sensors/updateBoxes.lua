local sensorInfo = {
	name = "updateBoxes",
	desc = "Updates busy/free status for BoDs",
	author = "Krabec, novotnyt",
	date = "2018-06-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spEcho = Spring.Echo

function shouldBeFree(box)
	local boxpos = Spring.GetUnitPosition(box)
	for i = 1,3 do 
		if (Vec3(boxpos):Distance(bb.fronts[i]) < 1500) then
			return false
		end
	end
	return true
end

return function()
	for key,value in pairs(bb.boxes) do
		if value == "busy" and Spring.GetUnitTransporter(key) == nil then
      --free boxes that are not in the battle
			if shouldBeFree(key) then
				bb.boxes[key] = "free"
			end
		end
	end
	return nil
end